//
//  MainVC.swift
//  12. DreamLister
//
//  Created by Milan Tomasovic on 5/25/17.
//  Copyright © 2017 Milan. All rights reserved.
//

import UIKit
import CoreData

// NSFetchedResultsControllerDelegate - radi direktno sa CoreData i sa mojim tableView, i to da bi nam bilo mnogo lakse da radimo sa fetched rezultatima koje dobijemo iz DB. Skladisti u memoriji onliko podataka koliko mu treba u isto vreme. Kada pristupimo rezultatima u DB-u, objekti su automatski smesteni u RAM.
class MainVC: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    // NSFetchedResultsController je napravljen samo za UITableView
    var controller: NSFetchedResultsController<Item>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
//        generateTestData()
        attemptFetch()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // uzimam broj sekcija koje se nalaze u controller-u
        if let sections = controller.sections {
            // uzimam jednu sekciju iz niza sekcija
            let sectionInfo = sections[section]
            return sectionInfo.numberOfObjects
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ItemCell
        configureCell(cell: cell, indexPath: indexPath)
        
        return cell
    }
    
    // configure cell se poziva sa dve lokacije unutar ovog VC-a
    // KADA ZAVRSIS, POGLEDAJ DA LI MOZE BEZ OVE FUNC
    func configureCell(cell: ItemCell, indexPath: IndexPath) {
        // iz controller-a uzmi objekat koji je sa odredjenim indexom
        let item = controller.object(at: indexPath)
        cell.configureCell(item: item)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // umesto zareza moze i WHERE
        // proveravam da li postoje objekti u controller-u
        // fetchedObjects je getter i vraca niz Item-a
        if let objs = controller.fetchedObjects, objs.count > 0 {
            let item = objs[indexPath.row]
            performSegue(withIdentifier: "ItemDetailsVC", sender: item)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("SEGUE IDENTIFIER: \(segue.identifier!)")
        
        if segue.identifier == "ItemDetailsVC" {
            if let destination = segue.destination as? ItemDetailsVC {
                // sender koji se salje se nalazi u performSegue, zato ovde ispitujes koji je sender u pitanju
                if let item = sender as? Item {
                    destination.itemToEdit = item
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let sections = controller.sections {
            return sections.count
        }
        
        return 0
    }
    
    // visina rows-a koji zelim da bude.
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    
    /***********************************************************
     CEO CODE ISPOD, KOJI SE ODNOSI NA DB, MOZE DA IDE U POSEBNU KLASU ILI DA URADIM EXTENSION
     NA TAJ NACIN MOGU DA KORISTIM OVO VISE PUTA, A I NE MORAM OVAKO DA PRLJAM VC
     *************************************************************/
    
    
    // pokusavam da dovucem podatak iz DB-a
    func attemptFetch() {
        // govorimo sta zelimo da uzmemo iz DB, i onda kazemo IDI PA UZMI
        // zelimo da uzmemo Item
        // uzeo je podatka iz db
        let fetchRequest:NSFetchRequest<Item> = Item.fetchRequest()
        // sortiranje podataka po datumu, tj. poslednji podatak
        // created je polje iz baze. po njemu sortiramo
        let dateSort = NSSortDescriptor(key: "created", ascending: false)
        //sortiram po ceni
        let priceSort = NSSortDescriptor(key: "price", ascending: true)
        //sortiram po title
        let titleSort = NSSortDescriptor(key: "title", ascending: true)
        
        // ovo je momenat koji u stvari utice na promenu item-a u listi
        if segment.selectedSegmentIndex == 0 {
            // sortiraj mi moje podatke po dateSort-u
            fetchRequest.sortDescriptors = [dateSort]
        } else if segment.selectedSegmentIndex == 1 {
            // sortiraj mi moje podatke po priceSort-u
            fetchRequest.sortDescriptors = [priceSort]
        }else if segment.selectedSegmentIndex == 2 {
            // sortiraj mi moje podatke po titleSort-u
            fetchRequest.sortDescriptors = [titleSort]
        }
        
        
        // sa ovim controllerom radim sa podacima koji su vraceni iz Core Data fetch results da bismo dali podatke UITAbleView-u
        // sa ovim kreiramo controller koji ce biti zaduzen za nas Item
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        
        // sa ovim govorimo donjim metodama sta da slusaju.
        // donje metode su beginUpdates, endUpdates
        controller.delegate = self
        
        self.controller = controller
        
        // fetch moze da ne uspe pa mora do
        do {
            // proveravam da li postoje vec neki podaci u DB.
            // ako ne postoje, generisi podatke
            // ako postoje, performFetch
            let count = try ad.persistentContainer.viewContext.count(for: fetchRequest)
            if count == 0 {
                generateTestData()
            }
            try controller.performFetch()
        } catch {
            let error = error as NSError
            print("\(error)")
        }
    }
    
    
    // func koja slusa promene na segmented kontroli.
    // svaki put kada neko klikne na neku vrednost u segment kontroli, onda se poziva ova func
    @IBAction func segmentChange(_ sender: Any) {
        attemptFetch()
        tableView.reloadData()
    }
    
    
    // kada god ce se table view update, ovo ce poceti da slusa promene i onda ce da ih handluje
    // ovo je nesto slicno kao tableView.reloadData()
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        // beginUpdates pocinje seriju metoda koje rade CRUD operacije
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        // endUpdates zavrsava seriju metoda koje rade CRUD operacije
        tableView.endUpdates()
    }
    
    // ovaj metod gleda da li je nastala promena. da li je u pitanju, insert, update, delete
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            // newIndexPath je nova putanja ili novo mesto u tabeli tj. nova celija
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath {
                let cell = tableView.cellForRow(at: indexPath) as! ItemCell
                configureCell(cell: cell, indexPath: (indexPath as IndexPath))
            }
            
            // ovo je momenat kada korisnik uzme jednu celiju i draguje je gore dole. u sustini menja mesto celijama.
            // u stvarnosti se desava da na starom mestu je brise, a pravi je na novom mestu
        case .move:
            // ovde radi delete
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            // ovde radi insert
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        }
    }
    
    // testiranje
    func generateTestData() {
        
        let item = Item(context: context)
        item.title = "MacBook Pro"
        item.price = 2300
        item.details = "Its cool"
        
        
        print("Generisao item 1")
        
        let item2 = Item(context: context)
        item2.title = "Bose Headphones"
        item2.price = 300
        item2.details = "Able to block out everyone with this noise cancelling headphones"
        
        
        print("Generisao item 2")
        
        let item3 = Item(context: context)
        item3.title = "Tesla Model S"
        item3.price = 110000
        item3.details = "This car is fucking awesome"
        
        
        print("Generisao item 3")
        
        
        // sada SNIMI u DB!!!
        ad.saveContext()
    }
}











































