//
//  ItemCell.swift
//  12. DreamLister
//
//  Created by Milan Tomasovic on 5/25/17.
//  Copyright © 2017 Milan. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {
    
    @IBOutlet weak var thumb: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var itemType: UILabel!
    
    func configureCell(item: Item) {
        print("Title: \(item.title!)")
        title.text = item.title
        price.text = "$\(item.price)"
        details.text = item.details
        itemType.text = item.toItemType?.type
        print("Item type: \(item.toItemType?.type ?? "Nema tipa")")
        thumb.image = item.toImage?.image as? UIImage
    }
}
