//
//  ItemDetailsVC.swift
//  12. DreamLister
//
//  Created by Milan Tomasovic on 5/27/17.
//  Copyright © 2017 Milan. All rights reserved.
//

import UIKit
import CoreData

class ItemDetailsVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {


    @IBOutlet weak var detailsField: CustomTextField!
    @IBOutlet weak var priceField: CustomTextField!
    @IBOutlet weak var titleField: CustomTextField!
    @IBOutlet weak var storePicker: UIPickerView!
    @IBOutlet weak var thumbImage: UIImageView!
    
    // CORE DATA - Store je Entitet u DB
    // svaki CoreData entitet mogu da pozovem ovako kao klasu
    var stores = [Store]()
    var itemTypes = [ItemType]()
    
    // Item je opcion, zato sto neki put kada dodjemo na ovaj view, ne znaci da zelimo da radimo neki update postojeceg Item-a vec mozda zelim da dodam novi Item
    // opcion zato sto ga nisam poslao preko segue sa predhodnog VC-a
    var itemToEdit: Item?
    
    // klasa koja prikazuje user interface za pravljenje slika i filmova na uredjajima. takodjer mozemo da je koristimo i odabir snimljenih slika i filmova u aplikaciji. sluzi za obradu korisnicke interakcije.
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // skidam naslov aplikacije u navigation controller-u kada se dodje na ovaj view.
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        
        storePicker.delegate = self
        storePicker.dataSource = self
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        
        getStores()
        getItemTypes()
        
        // provera da li radim edit, ili dodajem novi
        // ovaj itemToEdit je nesto sto saljem sa MainVC, i ako nije prazan, znaci da postoji Item koji zelim da promenim
        if itemToEdit != nil {
            loadItemData()
        }
        
    }
    
    
    // vrednost reda u picker-u
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if component == 0 {
        let store = stores[row]
        // Store class ima svoj atribut name
        // Pogledaj Store u data modelu
        return store.name
        }
        
        let type = itemTypes[row]
       return type.type
    }
    
    // broj redova
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return stores.count
        }
        return itemTypes.count
        
    }
    
    // broj kolona
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    // sta se desava kada se tapne na vrednost u picker-u
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        print("SELECTED ROW 0: \(stores[pickerView.selectedRow(inComponent: 0)].name!)")
        print("SELECTED ROW 1: \(itemTypes[pickerView.selectedRow(inComponent: 1)].type!)")
    }
    
    // izvuci imena storova
    func getStores() {
        let fetchRequest:NSFetchRequest<Store> = Store.fetchRequest()
        
        // fetch moze da ne uspe pa mora do
        do {
            // proveravam da li postoje vec neki podaci u DB.
            // ako ne postoje, generisi podatke
            // ako postoje, performFetch
            let count = try ad.persistentContainer.viewContext.count(for: fetchRequest)
            if count == 0 {
                createStores()
            }
            // punim stores niz za podacima iz DB-a
            self.stores = try context.fetch(fetchRequest)
            self.storePicker.reloadComponent(0)
        } catch {
            let error = error as NSError
            print("\(error)")
        }
    }
    
    // izvuci tipove item-a
    func getItemTypes() {
        let fetchRequest: NSFetchRequest<ItemType> = ItemType.fetchRequest()
        
        do {
            let count = try ad.persistentContainer.viewContext.count(for: fetchRequest)
            if count == 0 {
                createTypes()
            }
            self.itemTypes = try context.fetch(fetchRequest)
            self.storePicker.reloadComponent(1)
        }catch {
            let error = error as NSError
            print("\(error)")
        }
    }
    
    func createTypes() {
        let itemType = ItemType(context: context)
        itemType.type = "Electronics"
        
        let itemType1 = ItemType(context: context)
        itemType1.type = "Cars"
        
        let itemType2 = ItemType(context: context)
        itemType2.type = "Crazy"
        
        ad.saveContext()
    }
    
    func createStores() {
        let store = Store(context: context)
        store.name = "Best buy"
        let store2 = Store(context: context)
        store2.name = "Amazon"
        let store3 = Store(context: context)
        store3.name = "Yellow Pages"
        let store4 = Store(context: context)
        store4.name = "Tesla DeailerShip"
        let store5 = Store(context: context)
        store5.name = "Blue electronics"
        
        ad.saveContext()
    }
    
    
    @IBAction func savePressed(_ sender: UIButton) {
        
        // ne dozvoljavam prazan unos u DB
        guard titleField.text != "", priceField.text != "", detailsField.text != "" else {
            navigationController?.popViewController(animated: true)
            return
        }
        
        var item: Item!
        
        // moram da napravim objekat preko kojeg cu da radim sa image
        // image je entity sam za sebe u db
        let picture = Image(context: context)
        picture.image = thumbImage.image
        
        let type = ItemType(context: context)
        type.type = itemTypes[storePicker.selectedRow(inComponent: 1)].type
        print("VRACANJE: \(type.type!)")
        
        
        // moram da uradim ovu proveru, inace ako krenem da editujem neki postojeci podatak, onda ce da mi doda novi u isto vreme kada bude snimao promenu za stari.
        if itemToEdit == nil {
            item = Item(context: context)
        }else {
            item = itemToEdit
        }
        
        // gornji kod smo mogli da napisemo i kao
        // ako je itemToEdit == nil onda item-u dodeli Item(context: context) inace dodeli itemToEdit
        // let item = itemToEdit ?? Item(context: context)
        
        
        // Entity ITEM ima atribut toImage koji pokazuje na Entity IMAGE
        item.toImage = picture
        item.toItemType = type
        
        // prvo proveriti da li ima nesto u textfield-u
        if let title = titleField.text {
            item.title = title
        }
        
        if let price = priceField.text {
            // convert String to Double
            item.price = (price as NSString).doubleValue
        }
        
        if let details = detailsField.text {
            item.details = details
        }
        
        
        // pogledaj data model, i kada kliknes na item videces da imamo STORE relationship
        // relationship se zove toStore
        // inComponent nije row nego je column. Imamo samo jedan column, zato kazem nula - 0
        // stores[0] - primer sta ce storePicker.selectedRow(inComponent: 0) - vraca broj selektovanog reda u koloni ili komponenti koja je pod indexom NULA - 0
        // u sustini, vraca indeks selektovanog reda u componenti sa indexom 0
        item.toStore = stores[storePicker.selectedRow(inComponent: 0)]
        item.toItemType = itemTypes[storePicker.selectedRow(inComponent: 1)]
        
        print("ITEM.TOSTORE: \(item.toStore!)")
        print("ITEM.TOITEMTYPE: \(item.toItemType!)")
        print("ITEM TYPE: \(itemTypes[storePicker.selectedRow(inComponent: 1)].type!)")
        print("PRITISNUO SI DUGME: \(storePicker.selectedRow(inComponent: 0))")
        print("STORE POD NAZIVOM: \(stores[storePicker.selectedRow(inComponent: 0)].name!)")
        
        ad.saveContext()
        
        // remove current VC
        navigationController?.popViewController(animated: true)
    }
    
    // ucitava podatke u view-ove na VC-u ako imam item koji editujem
    func loadItemData() {
        if let item = itemToEdit {
            titleField.text = item.title
            priceField.text = String(item.price)
            detailsField.text = item.details
            thumbImage.image = item.toImage?.image as? UIImage
            
            // ovde namestam da mi picker view pokaze tacno na onaj store koji je podesen prvi put kada je napravljen item koji zelim da promenim
            // sa optional binding proveravam da li postoji za taj item toStore atribut
            // var index mi je neka vrsta counter-a
            // prolazim kroz niz imena koji se nalaze u stores-u
            if let store = item.toStore {
                var index = 0
                repeat {
                    // uzimam vrednost po vrednost iz niza stores i proveravam da li je ime koje se nalazi pod odredjenim indexom, jednako imenu koje se nalazi u item.toStore. ako jeste, onda namesti pickerView na tu vrednost
                    // stores punis u getStores() - func je malo iznad, oko 86 linije
                    // u s promenjivoj se nalazi objekat Store
                    let s = stores[index]
                    if s.name == store.name {
                        storePicker.selectRow(index, inComponent: 0, animated: true)
                        break
                    }
                    index += 1
                }while (index < stores.count)
            }
            
            if let type = item.toItemType {
                var index = 0
                repeat {
                    let t = itemTypes[index]
                    if t.type == type.type {
                        storePicker.selectRow(index, inComponent: 1, animated: true)
                    }
                    index += 1
                }while (index < itemTypes.count)
            }
        }
    }
    
    
    // brisanje item-a iz DB
    @IBAction func deletePressed(_ sender: UIBarButtonItem) {
        // prvo da uradimo proveru da li postoji item koji zelimo da obrisemo
        // dakle brisemo samo one iteme koji postoje
        
        if itemToEdit != nil {
            context.delete(itemToEdit!)
            ad.saveContext()
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addImage(_ sender: UIButton) {
        // modalno prikazuje VC. modalno znaci jedno preko drugog. preko jednog VC-a se prikazuje ovaj koji pozoves ovako
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    // postavljam imagePickerController
    // Tells the delegate that the user picked a still image or movie.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let img = info[UIImagePickerControllerOriginalImage] as? UIImage {
            thumbImage.image = img
        }
        
        // kada se slika izabere, moram da skinem taj prozor u kojem se bira slika
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    
    
    
}






























