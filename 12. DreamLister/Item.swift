//
//  Item.swift
//  12. DreamLister
//
//  Created by Milan Tomasovic on 5/25/17.
//  Copyright © 2017 Milan. All rights reserved.
//

import Foundation

extension Item {
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        self.created = NSDate()
    }
}
