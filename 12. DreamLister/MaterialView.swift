//
//  MaterialView.swift
//  12. DreamLister
//
//  Created by Milan Tomasovic on 5/25/17.
//  Copyright © 2017 Milan. All rights reserved.
//

import UIKit

// ovo je nesto se menjamo kroz property u Storyboard-u
private var materialKey = false

extension UIView {
    
    // @IBInspectable oznacava nesto sto mogu da selektujem u Storyboard-u
    // u ovom slucaju za UIView sam ubacio dodatno podesavanje pod nazivom Material Design
    // naziv Material Design je napravljen od ovog propertija, materialDesign, na prvom velikom slovu je samo pocepao reci i onda je napravio naziv
    // vraca Bool sto znaci da cu u podesavanjima imati On i Off
    @IBInspectable var materialDesign: Bool {
        // vracam trenutnu vrednost
        get {
            return materialKey
        }
        // postavljam vrednosti
        set {
            // newValue je On ili Off, True ili Falseb
            materialKey = newValue
            if materialKey {
                self.layer.masksToBounds = false
                self.layer.cornerRadius = 3.0
                self.layer.shadowOpacity = 0.8
                self.layer.shadowRadius = 3.0
                self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                self.layer.shadowColor = UIColor(red: 157/255, green: 157/255, blue: 157/255, alpha: 1.0).cgColor
            } else {
                self.layer.cornerRadius = 0
                self.layer.shadowOpacity = 0
                self.layer.shadowRadius = 0
                self.layer.shadowColor = nil
            }
        }
    }
}
